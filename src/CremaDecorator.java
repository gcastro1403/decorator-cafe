
public class CremaDecorator extends AdicionalesDecorator{
	
	private Integer precio = 10;

	public CremaDecorator(Cafe decoratedAdicional) {
		
		super(decoratedAdicional);
		
	}
	
	@Override
	public void descripcion() {
		
		decoratedAdicional.descripcion();
		configCrema();
	}
	
	@Override
	public Integer precio() {
		
		Integer precioCafe = decoratedAdicional.precio();
		Integer precioTotal = precioCafe + precio;
		System.out.println("Precio total con crema:" + precioTotal);
		return precioTotal;
	}
	
	
	private void configCrema() {
		
		System.out.println(" Se esta añadiendo la crema ");
	}
	
	

}
