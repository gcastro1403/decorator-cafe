
public class RunDecorator {

	public static void main(String[] args) {
		
		Cafe normal = new Normal();
		
		
		Cafe normalWithMilk = new Normal();
		normalWithMilk = new LecheDecorator(normalWithMilk);
		
		
		Cafe descafeinadoWithCrema = new Descafeinado();
		descafeinadoWithCrema = new CremaDecorator(descafeinadoWithCrema);
		
		
		Cafe descafeinadoWithAzucar = new Descafeinado();
		descafeinadoWithAzucar = new AzucarDecorator(descafeinadoWithAzucar);
		
		
		System.out.println("Cafe normal sin adicionales -->");
		normal.descripcion();
		
		System.out.println("Precio sin adicionales -->");
		normal.precio();
		
		System.out.println("descafeinado con leche -->");
		normalWithMilk.descripcion();
		
		System.out.println("Precio descafeinado con leche -->");
		normalWithMilk.precio();
		
		System.out.println(" descafeinado con crema -->");
		descafeinadoWithCrema.descripcion();
		
		System.out.println("precio descafeinado con crema");
		descafeinadoWithCrema.precio();
		
		System.out.println("Descafeinado con azucar");
		descafeinadoWithAzucar.descripcion();
		
		System.out.println("precio descafeinado con azucar");
		descafeinadoWithAzucar.precio();
		
		

	}

}
