
public class Normal implements Cafe{
	
	private Integer precio = 10;

	@Override
	public void descripcion() {
		
		System.out.println("Cafe normal de olla");
		
	}

	@Override
	public Integer precio() {
		
		System.out.println("Este cafe cuesta $" + precio);
		return precio;
		
	}
	
	
	
}
