
public class LecheDecorator extends AdicionalesDecorator{
	
	private Integer precio = 5;

	public LecheDecorator(Cafe decoratedAdicional) {
		
		super(decoratedAdicional);
		
	}
	
	
	@Override
	public void descripcion() {
		
		decoratedAdicional.descripcion();
		configLeche();
	}
	
	@Override
	public Integer precio() {
		
		Integer precioCafe = decoratedAdicional.precio();
		Integer precioTotal = precioCafe + precio;
		System.out.println("Precio total con leche:" + precioTotal);
		return precioTotal;
		
	}
	
	private void  configLeche() {
		
		System.out.println(" Se esta añadiendo leche ");
	}
	
	

}
