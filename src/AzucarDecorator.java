
public class AzucarDecorator extends AdicionalesDecorator{
	
	private Integer precio = 3;

	public AzucarDecorator(Cafe decoratedAdicional) {
		
		super(decoratedAdicional);
		
	}
	
	@Override
	public void descripcion() {
		
		decoratedAdicional.descripcion();
		configAzucar();
		
	}
	
	@Override
	public Integer precio() {
		
		Integer precioCafe = decoratedAdicional.precio();
		Integer precioTotal = precioCafe + precio;
		System.out.println("Precio total con azucar:" + precioTotal);
		return precioTotal;
	}
	
	
	private void configAzucar () {
		
		System.out.println(" Se esta añadiendo Azucar al cafe ");
	}
	
	
}
