
public class AdicionalesDecorator implements Cafe{

	protected Cafe decoratedAdicional;
	
	public AdicionalesDecorator( Cafe decoratedAdicional ) {
		
		this.decoratedAdicional = decoratedAdicional;
		
	}
	
	@Override
	public void descripcion() {
		
		this.decoratedAdicional.descripcion();
		
	}

	@Override
	public Integer precio() {
		
		return this.decoratedAdicional.precio();
		
	}

}
